import { Table,Row, Col, Container } from "reactstrap";
// import React, { useContext, useEffect, useState } from "react";
// import Controller from "./Controller";
import React, { useState, useEffect } from "react";


const Cites = () => {
  const [llista, setLlista] = useState([]);
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const apiUrl = `http://localhost:3001/api/citarandom`;
    fetch(apiUrl)
      .then((response) => response.json())
      .then((data) => {
        setLlista(data.list);
        setLoading(false);
      })
      .catch((error) => setError(true));
    
    console.log("llista");
    console.log(llista);
    console.log("peticion enviada");

  }, []);

  if (error) {
    return <h3>Se ha producido un error...</h3>;
  }


  if (loading) {
    // return <IconoCentrado className="fa fa-spinner fa-spin fa-2x fa-fw" />;
    return <h3>loading...</h3>;
  }


  const filas = llista.map(el => {
    const autor = el.autor;
    const texto = el.texto;
  
    return (
        <tr>
            <td>{autor}</td>
            <td>{texto}</td>
        </tr>
    )
})


  return (
    <>
    
      {filas}
    </>
  );
};

export default Cites;
