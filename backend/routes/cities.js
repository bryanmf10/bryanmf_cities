const citiesRoutes = (app, fs) => {
    const dataPath = './data/cities.json';

    const readFile = (callback, returnJson = false, filePath = dataPath, encoding = 'utf8') => {
        fs.readFile(filePath, encoding, (err, data) => {
            if (err) {
                throw err;
            }

            callback(returnJson ? JSON.parse(data) : data);
        });
    };

    app.get('/api/citarandom', (req, res) => {
        fs.readFile(dataPath, 'utf8', (err, data) => {
            if (err) {
                throw err;
            }

            // Pendiente el tamaño del json para hacer el random
            // el random va de 1 a 10
            const randomCite = Math.floor(Math.random() * 10) + 1;
            res.send(JSON.parse(data)[randomCite]);
        });
    });
};

module.exports = citiesRoutes;