const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();
app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

//inicialitzem mongoose
const db = require("mongoose");
db.Promise = global.Promise;

const conn_url = "mongodb://localhost:27017/bolets_db";

//connectem a la bdd
db.connect(conn_url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => {
        console.log("Connexió OK!");
    })
    .catch(err => {
        console.log("Connexió falla...", err);
        process.exit();
    });

db.set('useFindAndModify', false);

//creem esquema i model 'Bolet'
const boletCollection = db.Schema({ nom: String, color: String, temporada: String });
const Bolet = db.model('bolet', boletCollection);

const boletairesCollection = db.Schema({ nom: String, email: String, tel: String });
const Boletaire = db.model('boletaire', boletairesCollection);


// definim rutes de la API
app.get("/", (req, res) => {
    res.send("<h1>Benvingut a la API Boletaire!</h1>");
});

// consulta TOTS els bolets
app.get("/api/bolets", (req, res) => {
    Bolet.find()
        .then(data => {
            // hacemos cosas con los datos antes de enviar...
            res.status(200).json(data);
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});


// consulta UN bolet per ID
app.get("/api/bolets/:id", (req, res) => {
    const id = req.params.id;
    Bolet.findById(id)
        .then(data => {
            res.status(200).json(data);
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});


// crea un bolet
app.post("/api/bolets", (req, res) => {
    if (!req.body.nom) {
        res.status(400).send({ error: "No trobo el bolet!" });
        return;
    }

    // bolet nou
    const bolet = new Bolet({
        nom: req.body.nom,
        color: req.body.color,
    });

    // el guardem a la bdd

    bolet.save(bolet)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});


// modifica un bolet
app.put("/api/bolets/:id", (req, res) => {
    if (!req.body) {
        res.status(400).send({ error: "No trobo dades!" });
        return;
    }

    const id = req.params.id;

    Bolet.findByIdAndUpdate(id, req.body)
        .then(data => {
            if (!data) {
                res.send({error: "No s'ha actualitzat res"});
            } else {
                res.send({msg: "Actualitzat"});
            }
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});

//elimina un bolet
app.delete("/api/bolets/:id", (req, res) => {
    const id = req.params.id;
    Bolet.findByIdAndRemove(id)
        .then(data => {
            if (!data) {
                res.send({error: "No s'ha eliminat res"});
            } else {
                res.send({msg: "Eliminat", id: id });
            }
        })
        .catch(err => {
            res.status(500).send({
                error: err.message || "Alguna cosa falla."
            });
        });
});




// set port, listen for requests
const PORT = 8080;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});
